<?php
namespace Framework\Routing;
Router::addRoute(new \Framework\Routing\Route('hello', 'HelloController@index', Route::METHOD_GET));
Router::addRoute(new \Framework\Routing\Route('user/{user_name}/group/{group_name}', 'HelloController@data', Route::METHOD_GET));